#include <iostream>

#define GLEW_STATIC
#include <GL\glew.h>

#include "glloadshaders.h"

#include "Program.h"

//////////////////

//////////////////


void Program::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE)
	{
		glfwSetWindowShouldClose(window, true);
	}

	if (action == GLFW_PRESS)
	{
		keys[key] = true;
	}
	else if (action == GLFW_RELEASE)
	{
		keys[key] = false;
	}
}

void Program::cursorpos_callback(GLFWwindow* window, double xpos, double ypos)
{

}

void Program::error_callback(int error, const char* description)
{
	fprintf(stderr, "Error %i: %s\n", error, description);
}


void Program::start()
{
	scene = Scene(window);

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	GLfloat dtime;
	GLfloat lastframe = 0.0f;
	//getchar();
	glfwSetTime(0.0);
	while (!glfwWindowShouldClose(window))
	{
		GLfloat currentframe = static_cast<float>(glfwGetTime());
		dtime = currentframe - lastframe;
		lastframe = currentframe;

		glfwPollEvents();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//std::cout << dtime << "\n";

		scene.update(dtime * 10, keys);
		scene.draw();

		glfwSwapBuffers(window);
	}
}


Program::Program()
{	
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(1024, 768, "bq win", 0, 0);
	if (window == 0)
	{
		fprintf(stderr, "Failed to open window\n");
		getchar();
		glfwTerminate();
	}
	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
	}

	shaderprogram = LoadShaders("Shaders\\VertexShader.glsl", "Shaders\\FragmentShader.glsl");

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	// Callback functions

	glfwSetWindowUserPointer(window, this);
	auto l_key_cb = [](GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		static_cast<Program*>(glfwGetWindowUserPointer(window))->
			key_callback(window, key, scancode, action, mods); //Key callback func
	};
	glfwSetKeyCallback(window, l_key_cb);

	auto l_mousepos_cb = [](GLFWwindow* window, double xpos, double ypos)
	{
		static_cast<Program*>(glfwGetWindowUserPointer(window))->
			cursorpos_callback(window, xpos, ypos); //Cursor callback func
	};
	glfwSetCursorPosCallback(window, l_mousepos_cb);

	glfwSetErrorCallback(error_callback); //Error callback func


	glUseProgram(shaderprogram);

	Pixel::initStatic();
}

Program::~Program()
{
	//glDeleteProgram(shaderprogram);
	glfwTerminate();
}