﻿#include <iostream>
#include <cstdio>
#include <cstdlib>
#define GLEW_STATIC
#include <GL\glew.h>

#include "Layer.h"



void Layer::clustersInteraction()
{
	for (std::list<Cluster>::iterator clust_i = clusters.begin(); clust_i != clusters.end(); clust_i++)
	{
		for (std::list<Pixel>::iterator pix_i = clust_i->pixels.begin(); pix_i != clust_i->pixels.end(); pix_i++)
		{

		}
	}
}


void Layer::update(float dtime)
{
	for (std::list<Cluster>::iterator clust_i = clusters.begin(); clust_i != clusters.end(); clust_i++)
	{
		if (clust_i->pixels.back().position.y <= 0.0f)
		{
			clust_i->applyForce({ 0.0f, 10.0f, 0.0f });
		}
		clust_i->update(dtime);
	}
}

void Layer::draw()
{
	for (std::list<Cluster>::iterator clust_i = clusters.begin(); clust_i != clusters.end(); clust_i++)
	{
		clust_i->draw();
	}
}


Layer::Layer(const float& lay_) :
	lay(lay_)
{
	clusters.push_back(Cluster());
	for (int i = 4; i < 4 + 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			Pixel pix = Pixel(j, i, lay);
			float c = abs(sin(i * j));
			pix.setColor(c, 0.0f, 0.0f, 1.0f);
			clusters.back().addPixel(pix);
			//clusters.back().isStatic = true;
		}
	}
	clusters.back().applyForce({ -10.0f, 6.0f, 0.0f });

	clusters.push_back(Cluster());
	for (int i = 4; i < 4 + 6; i++)
	{
		for (int j = 7; j < 7 + 6; j++)
		{
			Pixel pix = Pixel(j, i, lay);
			float c = abs(sin(i * j));
			pix.setColor(0.0f, c, 0.0f, 1.0f);
			clusters.back().addPixel(pix);
			//clusters.back().isStatic = true;
		}
	}
	clusters.back().applyForce({ 5.0f, 20.0f, 0.0f });

	clusters.push_back(Cluster());
	for (int i = -32; i < 32; i++)
	{
		Pixel pix(i, -1.0f, lay);
		pix.setColor(0.5f, 0.5f, 0.5f);
		clusters.back().addPixel(pix);
		clusters.back().isStatic = true;
	}
}

Layer::~Layer()
{
}
