#pragma once

#include <vector>
#include <list>

#include <GLFW\glfw3.h>
#include <glm\glm.hpp>

#include "Cluster.h"


class Layer
{
public:

	void update(float dtime);
	void draw();

	void clustersInteraction();

	Layer(const float& lay);
	~Layer();


private:

	float lay;

	std::list<Cluster> clusters;
};

