pragma once


class ColorRGB
{
public:
	unsigned char r;
	unsigned char g;
	unsigned char b;
	
	friend ColorRGB operator+(const ColorRGB& c1, const ColorRGB& c2)
	{
		return ColorRGB(
			(c1.r + c2.r) > 255 ? 255 : (c1.r + c2.r),
			(c1.g + c2.g) > 255 ? 255 : (c1.g + c2.g),
			(c1.b + c2.b) > 255 ? 255 : (c1.b + c2.b));
	}

	friend ColorRGB operator-(const ColorRGB& c1, const ColorRGB& c2)
	{
		return ColorRGB(
			(c1.r - c2.r) < 0 ? 0 : (c1.r - c2.r),
			(c1.g - c2.g) < 0 ? 0 : (c1.g - c2.g),
			(c1.b - c2.b) < 0 ? 0 : (c1.b - c2.b));
	}

	ColorRGB(int r, int g, int b, int a) : r(r), g(g), b(b)
	{}
};

class ColorRGBA
{
public:
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;

	friend ColorRGBA operator+(const ColorRGBA& c1, const ColorRGBA& c2)
	{
		return ColorRGBA(
			(c1.r + c2.r) > 255 ? 255 : (c1.r + c2.r),
			(c1.g + c2.g) > 255 ? 255 : (c1.g + c2.g),
			(c1.b + c2.b) > 255 ? 255 : (c1.b + c2.b),
			(c1.a + c2.a) > 255 ? 255 : (c1.a + c2.a));
	}

	friend ColorRGBA operator-(const ColorRGBA& c1, const ColorRGBA& c2)
	{
		return ColorRGBA(
			(c1.r - c2.r) < 0 ? 0 : (c1.r - c2.r),
			(c1.g - c2.g) < 0 ? 0 : (c1.g - c2.g),
			(c1.b - c2.b) < 0 ? 0 : (c1.b - c2.b),
			(c1.a - c2.a) < 0 ? 0 : (c1.a - c2.a));
	}

	ColorRGBA(int r = 0, int g = 0, int b = 0, int a = 0) : r(r), g(g), b(b), a(a)
	{}
};
