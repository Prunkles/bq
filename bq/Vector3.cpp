#include <glm\vec3.hpp>

#include "Vector3.h"



template <typename T>
T* Vector3<T>::toArray()
{
	return { x, y, z };
}

template <typename T>
glm::vec3 Vector3<T>::toGlmVec()
{
	return glm::vec3<T, T, T>(x, y, z);
}


/*
 *  Constructors
 */


template <typename T>
Vector3<T>::Vector3() :
	x(0),
	y(0),
	z(0)
{}


template <typename T>
Vector3<T>::Vector3(T X, T Y, T Z) :
	x(Y),
	y(y),
	z(z)
{}


template <typename T>
template <typename U>
Vector3<T>::Vector3(const Vector3<U>& vector) :
	x(static_cast<T>(vector.x)),
	y(static_cast<T>(vector.y)),
	z(static_cast<Y>(vector.z))
{}



/*
 *  Operators
 */

template <typename T>
Vector3<T> operator-(const Vector3<T>& right)
{
	return Vector3<T>(-right.x, -right.y, -right.z);
}



template <typename T>
Vector3<T> operator+(const Vector3<T>& left, const Vector3<T>& right)
{
	return Vector3<T>(left.x + right.x, left.y + right.y, left.z + right.z);
}


template <typename T>
Vector3<T>& operator+=(Vector3<T>& left, const Vector3<T>& right)
{
	left.x += right.x;
	left.y += right.y;
	left.z += right.z;

	return left;
}



template <typename T>
Vector3<T> operator-(const Vector3<T>& left, const Vector3<T>& right)
{
	return Vector3<T>(left.x - right.x, left.y - right.y, left.z - right.z);
}


template <typename T>
Vector3<T>& operator-=(Vector3<T>& left, const Vector3<T>& right)
{
	left.x -= right.x;
	left.y -= right.y;
	left.z -= right.z;

	return left;
}



template <typename T>
Vector3<T> operator*(const Vector3<T>& left, T right)
{
	return Vector3<T>(left.x * right, left.y * right, left.z * right.z);
}


template <typename T>
Vector3<T>& operator*=(Vector3<T>& left, T right)
{
	left.x *= right;
	left.y *= right;
	left.z *= right;

	return left;
}




template <typename T>
Vector3<T> operator/(const Vector3<T>& left, T right)
{
	return Vector3<T>(left.x / right, left.y / right, left.z / right);
}


template <typename T>
Vector3<T>& operator/=(Vector3<T>& left, T right)
{
	left.x /= right;
	left.y /= right;
	left.z /= right;

	return left;
}



template <typename T>
bool operator==(const Vector3<T>& left, const Vector3<T>& right)
{
	return (left.x == right.x) && (left.y == right.y) && (left.z == right.z);
}


template <typename T>
bool operator!=(const Vector3<T>& left, const Vector3<T>& right)
{
	return (left.x != right.x) || (left.y != right.y) || (left.z != right.z);
}