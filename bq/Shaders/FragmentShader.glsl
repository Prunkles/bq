//Fragment Shader
#version 330 core

uniform vec4 pixcolor;

out vec4 color;

void main()
{
	color = pixcolor;
}