﻿#include <iostream>

#include "PhysConsts.h"

#include "Cluster.h"


void Cluster::applyForce(glm::vec3 force_)
{
	force += force_;
}


void Cluster::addPixel(Pixel pixel)
{
	pixels.push_back(pixel);
}


void Cluster::draw()
{
	for (std::list<Pixel>::iterator i = pixels.begin(); i != pixels.end(); i++)
	{
		i->draw();
	}
}

float Cluster::countMass()
{
	int mass_ = 0;
	for (std::list<Pixel>::iterator i = pixels.begin(); i != pixels.end(); i++)
	{
		mass_ += i->getMass();
	}
	return mass_;
}


void Cluster::update(float dtime)
{
	mass = countMass(); 
	
	acc = force / mass;

	if (!isStatic)
	{
		for (std::list<Pixel>::iterator pix_i = pixels.begin(); pix_i != pixels.end(); pix_i++)
		{
			pix_i->setPos(pix_i->getPos() + acc * dtime);
		}

		// Free fall acceleration
		force += glm::vec3(0.0f, -mass * GRAVITY_G, 0.0f);
	}
}


Cluster::Cluster()
{
	force = { 0.0f, 0.0f, 0.0f };
	acc = { 0.0f, 0.0f, 0.0f };
	isStatic = false;
}


Cluster::~Cluster()
{
}
