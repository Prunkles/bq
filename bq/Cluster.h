#pragma once

#include <list>

#include "Pixel.h"


class Cluster
{
public:

	bool isStatic;

	void applyForce(glm::vec3 force);

	void draw();

	void addPixel(Pixel pixel);

	void update(float dtime);

	Cluster();
	~Cluster();

	std::list<Pixel> pixels;

private:

	float countMass();

	float mass;

	glm::vec3 force;

	glm::vec3 acc;
};