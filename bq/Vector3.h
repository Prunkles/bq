#pragma once



template <typename T>
class Vector3
{
public:
	T x, y, z;

	T* toArray();

	glm::vec3 toGlmVec();

	Vector3();
	Vector3(T x, T y, T z);
	template <typename U>
	explicit Vector3(const Vector3<U>& vector);
};



template <typename T>
Vector3<T> operator-(const Vector3<T>& right);


template <typename T>
Vector3<T> operator+(const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T>& operator+=(const Vector3<T>& left, const Vector3<T>& right);


template <typename T>
Vector3<T> operator-(const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
Vector3<T>& operator-=(const Vector3<T>& left, const Vector3<T>& right);


template <typename T>
Vector3<T> operator*(const Vector3<T>& left, T right);

template <typename T>
Vector3<T>& operator*=(const Vector3<T>& left, T right);


template <typename T>
Vector3<T> operator/(const Vector3<T>& left, T right);

template <typename T>
Vector3<T>& operator/=(const Vector3<T>& left, T right);


template <typename T>
bool operator==(const Vector3<T>& left, const Vector3<T>& right);

template <typename T>
bool operator!=(const Vector3<T>& left, const Vector3<T>& right);


//#include "Vector3.inl"


typedef Vector3<int>			Vector3i;
typedef Vector3<unsigned int>	Vector3ui;
typedef Vector3<char>			Vector3b;
typedef Vector3<unsigned char>	Vector3ub;
typedef Vector3<float>			Vector3f;
typedef Vector3<double>			Vector3d;
typedef Vector3<long double>	Vector3l;