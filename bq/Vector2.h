#pragma once



template <typename T>
class Vector2
{
public:
	T x, y;

	T* toArray();

	glm::vec2 toGlmVec();

	Vector2();
	Vector2(T x, T y);
	template <typename U>
	explicit Vector2(const Vector2<U>& vector);
};


template <typename T>
Vector2<T> operator-(const Vector2<T>& right);


template <typename T>
Vector2<T> operator+(const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T>& operator+=(const Vector2<T>& left, const Vector2<T>& right);


template <typename T>
Vector2<T> operator-(const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
Vector2<T>& operator-=(const Vector2<T>& left, const Vector2<T>& right);


template <typename T>
Vector2<T> operator*(const Vector2<T>& left, T right);

template <typename T>
Vector2<T>& operator*=(const Vector2<T>& left, T right);


template <typename T>
Vector2<T> operator/(const Vector2<T>& left, T right);

template <typename T>
Vector2<T>& operator/=(const Vector2<T>& left, T right);


template <typename T>
bool operator==(const Vector2<T>& left, const Vector2<T>& right);

template <typename T>
bool operator!=(const Vector2<T>& left, const Vector2<T>& right);


//#include "Vector2.inl"


typedef Vector2<int>			Vector2i;
typedef Vector2<unsigned int>	Vector2ui;
typedef Vector2<char>			Vector2b;
typedef Vector2<unsigned char>	Vector2ub;
typedef Vector2<float>			Vector2f;
typedef Vector2<double>			Vector2d;
typedef Vector2<long double>	Vector2l;