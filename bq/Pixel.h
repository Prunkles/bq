#pragma once

#include <GLFW\glfw3.h>
#include <glm\glm.hpp>


class Pixel
{
public:

	void draw();

	void setColor(float r, float g, float b, float a);
	void setColor(float r, float g, float b);

	void setPos(float x, float y, float z);
	void setPos(glm::vec3 pos);

	glm::vec3 getPos();

	int getMass();

	static void initStatic();

	static void set3D(bool is3D);

	Pixel();
	Pixel(float x, float y, float z);

	~Pixel();

	glm::vec3 position;

private:
	
	float mass;


	glm::vec4 color;
	static GLuint color_loc;

	glm::mat4 model;
	static GLuint model_loc;

	static GLuint vao;

	static GLuint vbo;
	static GLfloat vbo_data[24];

	static GLuint ebo;
	static GLint ebo_data[14];

	static bool is3D;
};

