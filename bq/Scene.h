#pragma once

#include <vector>

#include "Layer.h"
#include "Camera.h"



class Scene
{
public:

	void update(GLfloat dtime, bool* keys);

	void draw();

	void mooveCamera(float dtime, bool* keys);

	void addLayer();

	Scene() {}
	Scene(GLFWwindow* window);
	~Scene();


private:

	std::vector<Layer> layers;

	Camera camera;

	int width;
	int height;

	void drawLayers();
};

