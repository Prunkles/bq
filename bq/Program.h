#pragma once

#include <GLFW\glfw3.h>

#include "Scene.h"



class Program
{
private:

	bool keys[1024];

	GLFWwindow* window;

	GLuint shaderprogram;

	Scene scene;

	
public:

	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	void cursorpos_callback(GLFWwindow* window, double xpos, double ypos);
	static void error_callback(int error, const char* description);

	void start();

	Program();
	~Program();

};