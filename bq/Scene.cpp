#include <iostream>

#define GLEW_STATIC
#include <GL\glew.h>

#include "Scene.h"



void Scene::drawLayers()
{
	for (int i = 0; i < layers.size(); i++)
	{
		layers[i].draw();
	}

}


void Scene::draw()
{
	drawLayers();
}


void Scene::addLayer()
{
	layers.push_back(Layer(layers.size()));
}


void Scene::mooveCamera(float dtime, bool* keys)
{
	if (keys[GLFW_KEY_A])
	{
		camera.setPos(camera.getPos() + glm::vec3(-1.0f, 0.0f, 0.0f) * dtime);
	}
	if (keys[GLFW_KEY_D])
	{
		camera.setPos(camera.getPos() + glm::vec3(+1.0f, 0.0f, 0.0f) * dtime);
	}
	if (keys[GLFW_KEY_S])
	{
		camera.setPos(camera.getPos() + glm::vec3(0.0f, -1.0f, 0.0f) * dtime);
	}
	if (keys[GLFW_KEY_W])
	{
		camera.setPos(camera.getPos() + glm::vec3(0.0f, +1.0f, 0.0f) * dtime);
	}
	if (keys[GLFW_KEY_UP])
	{
		camera.setPos(camera.getPos() + glm::vec3(0.0f, 0.0f, -1.0f) * dtime);
	}
	if (keys[GLFW_KEY_DOWN])
	{
		camera.setPos(camera.getPos() + glm::vec3(0.0f, 0.0f, +1.0f) * dtime);
	}
	camera.setLookPoint((camera.getPos() + glm::vec3(0.0f, 0.0f, -1.0f)));

	if (keys[GLFW_KEY_Z])
	{
		Pixel::set3D(true);
	}
	if (keys[GLFW_KEY_X])
	{
		Pixel::set3D(false);
	}
}


void Scene::update(GLfloat dtime, bool* keys)
{
	for (std::vector<Layer>::iterator layer_i = layers.begin(); layer_i != layers.end(); layer_i++)
	{
		layer_i->update(dtime);
	}
	mooveCamera(dtime, keys);
}


Scene::Scene(GLFWwindow* window)
{
	camera = Camera(window);
	camera.setPerspective(glm::radians(45.0f), 0.1f, 1000.0f);
	camera.setPos(0.0f, 0.0f, 64.0f);

	for (int i = 0; i < 1; i++)
	{
		addLayer();
	}
}

Scene::~Scene()
{
}