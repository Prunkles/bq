#include <iostream>

#define GLEW_STATIC
#include <GL\glew.h>

#include <glm/gtc/matrix_transform.hpp>

#include "Pixel.h"



#pragma region static

GLuint Pixel::vao;

GLuint Pixel::vbo;
GLfloat Pixel::vbo_data[24] = {
	-0.5f, 0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,
	-0.5f, -0.5f, 0.5f,
	0.5f, -0.5f, 0.5f,
	-0.5f, 0.5f, -0.5f,
	0.5f, 0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f
};

GLuint Pixel::ebo;
GLint Pixel::ebo_data[14] = { 0, 1, 2, 3, 7, 1, 5, 0, 4, 2, 6, 7, 4, 5 };

GLuint Pixel::color_loc;
GLuint Pixel::model_loc;

bool Pixel::is3D;

void Pixel::initStatic()
{
	// Vertecies
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ebo_data), ebo_data, GL_STATIC_DRAW);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 8 * 3, vbo_data, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	// Model locations
	GLint shaderprogram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &shaderprogram);
	
	color_loc = glGetUniformLocation(shaderprogram, "pixcolor");
	model_loc = glGetUniformLocation(shaderprogram, "model");
}


void Pixel::set3D(bool is3D)
{
	Pixel::is3D = is3D;
}

#pragma endregion


void Pixel::setColor(float r, float g, float b, float a)
{
	color = { r, g, b, a };
}
void Pixel::setColor(float r, float g, float b)
{
	setColor(r, g, b, 1.0f);
}


void Pixel::setPos(glm::vec3 pos)
{
	position = pos;
	model = glm::translate(glm::mat4(), is3D || 0 ? glm::round(position) : position);
}
void Pixel::setPos(float x, float y, float z)
{
	setPos({ x, y, z });
}

glm::vec3 Pixel::getPos()
{
	return position;
}

int Pixel::getMass()
{
	return mass;
}


void Pixel::draw()
{
	glBindVertexArray(vao);

	glUniform4fv(color_loc, 1, &color[0]);
	glUniformMatrix4fv(model_loc, 1, GL_FALSE, &model[0][0]);

	glEnableVertexAttribArray(0);
	if (is3D)
	{
		glDrawElements(GL_TRIANGLE_STRIP, 14, GL_UNSIGNED_INT, 0);
	}
	else
	{
		glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, 0);
	}
	glDisableVertexAttribArray(0);

	glBindVertexArray(0);
}


Pixel::Pixel(float x, float y, float z)
{
	mass = 1;
	setPos(x, y, z);
}
Pixel::Pixel() {}

Pixel::~Pixel()
{
}
