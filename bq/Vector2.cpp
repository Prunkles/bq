#include <glm\vec2.hpp>

#include "Vector2.h"


template <typename T>
T* Vector2<T>::toArray()
{
	return { x, y };
}

template <typename T>
glm::vec2 Vector2<T>::toGlmVec()
{
	//return glm::vec2(x, y);
}


/*
 *  Constructors
 */


template <typename T>
 Vector2<T>::Vector2() :
	x(0),
	y(0)
{}


template <typename T>
Vector2<T>::Vector2(T X, T Y) :
	x(x),
	y(y)
{}


template <typename T>
template <typename U>
 Vector2<T>::Vector2(const Vector2<U>& vector) :
	x(static_cast<T>(vector.x)),
	y(static_cast<T>(vector.y))
{}



/*
 *  Operators
 */


template <typename T>
 Vector2<T> operator-(const Vector2<T>& right)
{
	return Vector2<T>(-right.x, -right.y);
}



template <typename T>
 Vector2<T> operator+(const Vector2<T>& left, const Vector2<T>& right)
{
	return Vector2<T>(left.x + right.x, left.y + right.y);
}


template <typename T>
 Vector2<T>& operator+=(Vector2<T>& left, const Vector2<T>& right)
{
	left.x += right.x;
	left.y += right.y;

	return left;
}



template <typename T>
 Vector2<T> operator-(const Vector2<T>& left, const Vector2<T>& right)
{
	return Vector2<T>(left.x - right.x, left.y - right.y);
}


template <typename T>
 Vector2<T>& operator-=(Vector2<T>& left, const Vector2<T>& right)
{
	left.x -= right.x;
	left.y -= right.y;

	return left;
}



template <typename T>
 Vector2<T> operator*(const Vector2<T>& left, T right)
{
	return Vector2<T>(left.x * right, left.y * right);
}


template <typename T>
 Vector2<T>& operator*=(Vector2<T>& left, T right)
{
	left.x *= right;
	left.y *= right;

	return left;
}




template <typename T>
 Vector2<T> operator/(const Vector2<T>& left, T right)
{
	return Vector2<T>(left.x / right, left.y / right);
}


template <typename T>
 Vector2<T>& operator/=(Vector2<T>& left, T right)
{
	left.x /= right;
	left.y /= right;

	return left;
}



template <typename T>
 bool operator==(const Vector2<T>& left, const Vector2<T>& right)
{
	return (left.x == right.x) && (left.y == right.y);
}


template <typename T>
 bool operator!=(const Vector2<T>& left, const Vector2<T>& right)
{
	return (left.x != right.x) || (left.y != right.y);
}